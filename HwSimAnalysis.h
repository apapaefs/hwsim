
bool HwSim::BasicCutsPass(int numparticles, double objects[8][10000], vector<fastjet::PseudoJet> jet, vector<fastjet::PseudoJet> photon, vector<fastjet::PseudoJet> lepton, vector<int> lepton_id, fastjet::PseudoJet pmiss, double cut_eta, double cut_pt_part, double cut_pt_jet, double cut_eta_jet, double cut_pt_muon, double cut_eta_muon, double cut_pt_electron, double cut_eta_electron) {
  
  /* COUNTERS and BOOLEANS
   * for number of events 
   * that pass cuts
   */
  //COUNTERS

  //BOOLEANS
  bool passed_bjets(true);
  bool passed_nlightjets(true);
  bool passed_deltarbb(true);
  bool passed_mbb(true);
  bool passed_met(true);

  /* PARAMETERS & SWITCHES 
   * DEFINED HERE
   */

  double pt_bjet1_min(40.);
  double pt_bjet2_min(40.);
  
  double mbb_min(100.);
  double mbb_max(150.);

  double deltarbb12_min(0.);
  double deltarbb12_max(1E5);

  double cut_met(0.);

  int nlightjets = 0;
  //find the bjets
  vector<fastjet::PseudoJet> bjets;
  for(int z = 0; z < jet.size(); z++) {
    if(btag_hadrons(jet[z])) {
      bjets.push_back(jet[z]);
    }
    else { nlightjets++; }
  }
  
  //make sure there are at least two b-jets
  //reconstruct the invariant mass of the two hardest b-jets
  //this is only OK for the irreducible backgrounds!
  if(bjets.size()<2) { passed_bjets = false; }
  if(!passed_bjets) { return 0; }


  //and veto events without at least two extra light jets beyond the b-jets
  if(nlightjets<2) { passed_nlightjets = false; }
  
  double mbb12(-1.);
  if(bjets.size()>1) {
    mbb12 = (bjets[0]+bjets[1]).m();
    if(bjets[0].perp() < pt_bjet1_min) { passed_bjets = false; }
    if(bjets[1].perp() < pt_bjet2_min) { passed_bjets = false; }
  }
  if(mbb12 < mbb_min || mbb12 > mbb_max) { passed_mbb = false; }

  double deltarbb12 = deltaR(bjets[0], bjets[1]);
  if(deltarbb12 < deltarbb12_min || deltarbb12 > deltarbb12_max) { passed_deltarbb = false; }
  
  if(pmiss.perp() > cut_met) { passed_met = true; }   

  if(!passed_met) { return 0; }
  if(!passed_nlightjets) { return 0; }
  if(!passed_mbb) { return 0; }
  if(!passed_deltarbb) { return 0; }

 
  /* FOR EVENTS THAT PASS CUTS
   * PRINT INFORMATION
   * SET THE FLAG AS PASSED
   * DO EXTRA STUFF (e.g. PLOTS)
   */
  return true;
}

