import ROOT
import sys
import numpy as np
import math
import random
from matplotlib import colors
import matplotlib.gridspec as gridspec # more plotting 
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
from tqdm import tqdm
import multiprocessing
import time
from time import sleep
import logging
from functools import partial
tqdm = partial(tqdm, position=0, leave=True)
import fastjet

##########################
# VARIABLES
##########################

# Cuts for jets 
jetPtMin    = 10
jetEtaMax   = 5  # HGCAL is up to 3

# cuts for particles to enter jet algorithm:
jcPtMin = 0.1
jcEtaMax = 6.0

# Debug
debug = False

##########################
# FUNCTIONS
##########################

# choose the next colour -- for plotting
ccount = 0
def next_color():
    global ccount
    colors = ['green', 'orange', 'red', 'blue', 'black', 'cyan', 'magenta', 'brown', 'violet'] # 9 colours
    color_chosen = colors[ccount]
    if ccount < 8:
        ccount = ccount + 1
    else:
        ccount = 0    
    return color_chosen

# do not increment colour in this case:
def same_color():
    global ccount
    colors = ['green', 'orange', 'red', 'blue', 'black', 'cyan', 'magenta', 'brown', 'violet'] # 9 colours
    color_chosen = colors[ccount-1]
    return color_chosen

# reset the color counter:
def reset_color():
    global ccount
    ccount = 0

# get momentum of ith particle in format
# E, px, py, pz, id
def ith_momentum(obj, ith):
    return (obj[0*10000+ith],obj[1*10000+ith],obj[2*10000+ith],obj[3*10000+ith],int(obj[4*10000+ith]))

# get all the momenta given the HwSim object
def get_momenta(obj, numprtcl):
    mom = []
    for i in range(0,numprtcl):
        mom.append(ith_momentum(obj, i))
    return np.array(mom, dtype=[('E', 'f8'), ('px', 'f8'), ('py', 'f8'), ('pz', 'f8'), ('id', 'int')])


def perp(p):
    pt = math.sqrt( p[1]**2 + p[2]**2 )
    return pt

def rapidity(p):
    rapd = 0.5 * math.log( (p[0] + p[3]) / (p[0] - p[3]) )
    return rapd

def convert_to_array(pseudojet):
    return [pseudojet.e, pseudojet.px, pseudojet.py, pseudojet.pz]
    
def add_4momenta(p1, p2):
    summedvec = []
    for (item1, item2) in zip(p1, p2):
        summedvec.append(item1 + item2)
    return summedvec

def invmass(fourvector):
    return math.sqrt(fourvector[0]**2 - fourvector[1]**2 - fourvector[2]**2 - fourvector[3]**2)


# plot histogram
def histogram(DATA, plotname, xlabel, nbins=50):
    print('---')
    print('plotting')

    # plot settings ########
    plot_type = plotname # the name of the plot
    # the following labels are in LaTeX, but instead of a single slash, two "\\" are required.
    ylab = 'fraction/bin' # the ylabel
    xlab = xlabel # the x label
    # log scale?
    ylog = False # whether to plot y in log scale
    xlog = False # whether to plot x in log scale

    # construct the axes for the plot
    # no need to modify this if you just need one plot
    gs = gridspec.GridSpec(4, 4)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)

    #print('len(DATA)=', len(DATA))
    bins, edges = np.histogram(np.array(DATA), bins=nbins)
    #print(bins)
    errors = np.divide(np.sqrt(bins), bins, out=np.zeros_like(np.sqrt(bins)), where=bins!=0.)
    #print errors
    bins = bins/float(len(DATA))
    errors = bins*errors
    print(bins)
    print(errors)
    left,right = edges[:-1],edges[1:]
    X = np.array([left,right]).T.flatten()
    Y = np.array([bins,bins]).T.flatten()
    #print('X=',X)
    #print('Y=',Y)
    plt.plot(X,Y, label='', color='red', lw=1)
    center = (edges[:-1] + edges[1:]) / 2
    plt.errorbar(center, bins, yerr=errors, color='red', lw=0, elinewidth=1, capsize=1)
    

    # set the ticks, labels and limits etc.
    ax.set_ylabel(ylab, fontsize=20)
    ax.set_xlabel(xlab, fontsize=20)
    
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    if xlog:
        ax.set_xscale('log')
    else:
        ax.set_xscale('linear')

    # set the limits on the x and y axes if required below:
    #xmin = 0.
    #xmax = 1500.
    #ymin = 0.
    #ymax = 0.09
    #plt.xlim([0,180])
    #plt.ylim([0.08,0.12])

    # create legend and plot/font size
    #ax.legend()
    #ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    print('output in', infile.replace('.dat','.pdf'))
    plt.savefig(infile.replace('.dat','.pdf'), bbox_inches='tight')
    plt.close(fig)


# plot histogram
def histogram_multi(DATA_array, plot_type, plotnames_multi, xlabel, nbins=50, xmin=0, xmax=180, ymin=0.06, ymax=0.15):
    print('---')
    print('plotting')

    # plot settings ########
    # the following labels are in LaTeX, but instead of a single slash, two "\\" are required.
    ylab = 'fraction/bin' # the ylabel
    xlab = xlabel # the x label
    # log scale?
    ylog = False # whether to plot y in log scale
    xlog = False # whether to plot x in log scale

    # construct the axes for the plot
    # no need to modify this if you just need one plot
    gs = gridspec.GridSpec(4, 4)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)

    #print('len(DATA)=', len(DATA))
    dd = 0
    for DATA in DATA_array:
        bins, edges = np.histogram(np.array(DATA), bins=nbins)
        #print(bins)
        errors = np.divide(np.sqrt(bins), bins, out=np.zeros_like(np.sqrt(bins)), where=bins!=0.)
        #print errors
        bins = bins/float(len(DATA))
        errors = bins*errors
        print(bins)
        print(errors)
        left,right = edges[:-1],edges[1:]
        X = np.array([left,right]).T.flatten()
        Y = np.array([bins,bins]).T.flatten()
        #print('X=',X)
        #print('Y=',Y)
        plt.plot(X,Y, label=plotnames_multi[dd], color=next_color(), lw=1)
        center = (edges[:-1] + edges[1:]) / 2
        plt.errorbar(center, bins, yerr=errors, color=same_color(), lw=0, elinewidth=1, capsize=1)
        dd = dd+1
    

    # set the ticks, labels and limits etc.
    ax.set_ylabel(ylab, fontsize=20)
    ax.set_xlabel(xlab, fontsize=20)
    
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    if xlog:
        ax.set_xscale('log')
    else:
        ax.set_xscale('linear')

    # set the limits on the x and y axes if required below:
    #xmin = 0.
    #xmax = 1500.
    #ymin = 0.
    #ymax = 0.09
    plt.xlim([xmin,xmax])
    plt.ylim([ymin,ymax])

    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    print('output in', infile.replace('.dat','.pdf'))
    plt.savefig(infile.replace('.dat','.pdf'), bbox_inches='tight')
    plt.close(fig)
    reset_color()


def get_events(treein, filename, maxevents=1000000):
    if maxevents > treein.GetEntries():
        maxevents = treein.GetEntries()
    print('Getting', maxevents, 'events from', filename)
    events = []
    for entryNum in tqdm(range(0,maxevents)):
        # get the entry from the tree
        treein.GetEntry(entryNum)
        # get the number of particles in the event
        # and the objects array
        numevents = getattr(treein,"numparticles")
        objects = getattr(treein,"objects")
        # convert the objects array to the right format
        momenta = get_momenta(objects, numevents)
        events.append(momenta)
    return events

# convert to fastjet, but only if the pt is > minptc
def convert_tofj(momin, minptc, maxrapc):
    arrayout = []
    for mm in range(len(momin)):
        #print(momin[mm][1], momin[mm][2], momin[mm][3], momin[mm][0])
        fj = fastjet.PseudoJet(momin[mm][1], momin[mm][2], momin[mm][3], momin[mm][0])
        if fj.perp() > minptc and abs(fj.eta()) < maxrapc:
            arrayout.append(fj)
        fj.set_user_index(int(momin[mm][4]))
    return arrayout

def get_clusters(events, filename, jetalgo, jetR, maxevents=100000):
    if maxevents > len(events):
        maxevents = len(events)
    print('Analyzing', maxevents, 'events from', filename)
    # put the Higgs momenta into an array:
    higgs = []
    # return the cluster:
    clusters_jets = []
    # return the unclustered objects:
    unclustered = []
    # jet algorithm
    jetdef = fastjet.JetDefinition(jetalgo, jetR)
    # loop over events and analyze:
    for yy in tqdm(range(0,maxevents)):
        # put the momenta for clustering into array:
        momtocluster = []
        # and the rest into another array;
        momNOcluster = []
        # all the momenta from this event
        momenta = events[yy]
        #print(momenta)
        for mm in range(0,len(momenta)):
            if momenta[mm][4] == 25: # find a Higgs boson
             higgs.append(momenta[mm])
             #print('Higgs boson found')
            if momenta[mm][4] != 25 and abs(momenta[mm][4]) != 12 and abs(momenta[mm][4]) != 14 and abs(momenta[mm][4]) != 16 and abs(momenta[mm][4]) != 11 and abs(momenta[mm][4]) != 13:
                momtocluster.append(momenta[mm])
            else:
                momNOcluster.append(momenta[mm])
        momfj = convert_tofj(momtocluster, jcPtMin,jcEtaMax)
        momfj_unclustered = convert_tofj(momNOcluster, jcPtMin,jcEtaMax)

        cluster = fastjet.ClusterSequence(momfj, jetdef)
        clusters_jets.append(cluster)
        unclustered.append(momfj_unclustered)
    return clusters_jets, unclustered

# from https://gitlab.cern.ch/cms-sw/cmssw/blob/e303d9f2c3d4f25397db5feb7ad59d2f20c842f2/PhysicsTools/HeppyCore/python/utils/deltar.py
def deltaPhi( p1, p2):
    '''Computes delta phi, handling periodic limit conditions.'''
    res = p1 - p2
    while res > np.pi:
        res -= 2*np.pi
    while res < -np.pi:
        res += 2*np.pi
    return res

##########################
# MAIN ANALYSIS FUNCTION #
##########################
def analyze(clusters_jets, unclustered):
    # define dictionary with data to return at the end of the analysis
    DATA = {}
    # example variables:
    DATA['ptleptons'] = []
    DATA['ptjets'] = []
    passed = 0
    # loop over events
    for ee in tqdm(range(0,len(clusters_jets))):
        # sort the jets, with jet pt minimum jetPtMin
        events_jets = fastjet.sorted_by_pt(clusters_jets[ee].inclusive_jets(jetPtMin))
        for jet in events_jets:
            if abs(jet.eta()) < jetEtaMax:
                DATA['ptjets'].append(jet.perp())
        for particle in unclustered[ee]:
            if abs(particle.user_index()) == 11 or abs(particle.user_index()) == 13:
                DATA['ptleptons'].append(particle.perp())
        passed = passed + 1
    print('Passed cuts fraction =', passed/len(clusters_jets))
    return DATA
    #histogram(relpull_array, 'relpull_' + plotname, 'Relative pull angle $\\theta_{12}$ (degrees)', nbins=10)

###########################
# MAIN PROGRAM
###########################

if len(sys.argv) < 2:
    print("USAGE: %s <input ROOT file(s)>"%(sys.argv[0]))
    sys.exit(1)

inFileName = sys.argv[1]
plotnames_multi = []
plotnames_multi.append(inFileName)

# read the ROOT file and get the data
inFile = ROOT.TFile.Open(inFileName ,"READ")
tree = inFile.Get("Data")

# analyze
print("Analyzing", inFileName)
print(inFileName, "contains:", tree.GetEntries(), "events")

# maximum number of events to analyze:
Nmax = 100000

# get all the event momenta from the root file:
events = get_events(tree, inFileName, maxevents=Nmax)


# jet algorithm radius parameter
R=0.4

# convert to jets
# in this case we ignore the (stable) Higgs boson and neutrinos
cluster_jets, unclustered = get_clusters(events, inFileName, fastjet.antikt_algorithm, R,maxevents=Nmax)

# analyze the jets in the events
OUTPUT = analyze(cluster_jets, unclustered)

# histogram: 
histogram(OUTPUT['ptleptons'], 'ptleptons', '$p_T$ of leptons [GeV]', nbins=10)
histogram(OUTPUT['ptjets'], 'ptjets', '$p_T$ of jets [GeV]', nbins=10)
