## Event-by-event Analysis using the HwSim Herwig Add-On

## Pre-reqs:

- Herwig 7: https://herwig.hepforge.org. (It is recommended that you use the herwig-bootstrap script, found here: https://herwig.hepforge.org/downloads/herwig-bootstrap)
- FastJet: http://fastjet.fr/ (Should be included in Herwig if installed through the herwig-bootsrap.)
- ROOT: https://root.cern.ch

## Description: 
- HwSim provides a way to analyze the events is to output them in a custom ```ROOT``` file format via the HwSim package. 

## Installation and Usage instructions:
- You will need to compile this as a Herwig 7 library first. Place the repository in the ```Herwig-installation-directory/src/Herwig-7.x.x/Contrib/``` and type ```make``` there. 
- Then cd into ```Herwig-installation-directory/src/Herwig-7.x.x/Contrib/HwSim``` and type ```make; make install```
- Example of the HwSim interface: read and run the ```LHC-HwSim.in``` file.
- This will generate a file  ```LHC-HwSim.root```, which will contain information that we will use for the analysis. 

## Analyzing the generated files:
- To analyze the file, use the analysis code in the HwSim/Analysis directory: compile with ```make``` and use ```./HwSimPostAnalysis_XXX LHC-HwSim.root``` to loop over the events and construct distributions. 
- There are three example C++ analyses: 
    - ```HwSimPostAnalysis_objects``` runs over all the final-state particles. 
    - ```HwSimPostAnalysis_objects_jetfinding``` runs over all the final-state particles and includes jet-finding.
    - ```HwSimPostAnalysis_reconstructed``` runs over all RECONSTRUCTED objects (jets, photons, leptons). 
For the first two, one has to make sure that the ```SaveObjects``` option is set to ```Yes``` and for the third that the ```SaveReconstructed``` option is set to ```Yes```.
- There is also a preliminary version of a Python alaysis:
    - ```HwSimPythonAnalysis``` currently analyzes the ```objects``` array (so make sure ```SaveObjects``` is set to ```Yes```) and does basic jet finding. 

## Output of the Analysis
- The output of the analysis will be a ```.top``` file, which contains some histograms in (almost) plain text format. 
- To plot directly using these files, check out https://gitlab.com/apapaefs/topydrawer, which is a python 3 interpreter for topdrawer files. 

